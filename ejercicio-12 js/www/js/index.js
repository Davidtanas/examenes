const fetchUrl = async (page) => {
    const response = await fetch(`https://rickandmortyapi.com/api/episode?page=${page}`);
    const result = await response.json();
    return result;
}

const getEpisodes = async() => {
    try {
        let page = 1;
        let {results, info} = await fetchUrl(page)
        // array.reduce
        for (page = 2; page <= info.pages; page++) {
            const {results: newResults} = await fetchUrl(page)
            results = [...results, ...newResults]
        }
        // con busco cualquier january en air date si me devuelve true se incluye
        const januaryEpisodes = results.filter(({air_date}) => /January/g.test(air_date)) 
        // obtain array of strings from array of objects
        const episodeNames = januaryEpisodes.map(({name}) => name)
        console.log(episodeNames)


    } catch (error) {
        console.log(error);
    }


}

getEpisodes();
