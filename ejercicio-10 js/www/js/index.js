'use strict';


//-------------------------------------------
/* 
 function binaryToDecimal(num) {
	const input = String(num);
	const binary = String(num)
	  .split('')
	  .filter(i => i === '1' || i === '0');
  
	if (input.length !== binary.length) {
	  return 'Invalid input';
	}
  
	return binary.reduce((acc, curr, index, arr) => {
	  return acc + Number(curr) * Math.pow(2, arr.length - 1 - index);
	}, 0);
	
  }
  
  */

const binary2Decimal = (number) => {
	const input = String(number).split("").reverse();
	// '101010001'
	// ['1', '0', '1']

	// 1 * 2^0 = 1 ----- 0
	// 0 * 2^1 = 0 ----- 1
	// 1 * 2^2 = 4 --- 1
	// 1 * 2^3 = 8 ---- 5
	// 1 * 2^8 = 256 ---- 13
	// 269

	return input.reduce((acc, current, index) => {
		// acc + 1 * 2^0
		// acc + (current * (2^index))
		return acc + (current * Math.pow(2, index))
	}, 0)
}


function convert(base, num) {
	if (base === 2) {
		return Number((num >>> 0).toString(2));
	} else if ( base === 10) {
		return binary2Decimal(num);
	} 
	throw new Error("Base desconocida")
}

module.exports = convert;

convert(10, 11111); 