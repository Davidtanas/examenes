const convert = require("../index")

describe("de binario a decimal", ()=>{
    it("deberia devolver 4 si el parametro es 100",()=>{
        expect(convert(10, 100)).toBe(4)
    })
    it("deberia devolver 0 si el parametro es 0",()=>{
        expect(convert(10, 0)).toBe(0)
    })
    it("deberia devolver 256 si el parametro es 10000000",()=>{
        expect(convert(10, 100000000)).toBe(256)
    })
    it("deberia devolver 472 si el parametro es 11001100",()=>{
        expect(convert(10, 111011000)).toBe(472)
    })
})
describe("de decimal a binario", ()=>{
    it("deberia devolver 100 si el parametro es 4",()=>{
        expect(convert(2, 4)).toBe(100)
    })
    it("deberia devolver 0 si el parametro es 0",()=>{
        expect(convert(2, 0)).toBe(0)
    })
    it("deberia devolver 256 si el parametro es 100000000",()=>{
        expect(convert(2, 256)).toBe(100000000)
    })
    it("deberia devolver 111011000 si el parametro es 472",()=>{
        expect(convert(2, 472)).toBe(111011000)
    })
})

describe("base desconocida", ()=>{
    it("deberia lanzar un error si la base es 4",()=>{
        expect(() => convert(4, 2)).toThrowError("Base desconocida")
    })
})