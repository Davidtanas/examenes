'use strict';


const fetchUrl = async (users) => {
    try {
        const response = await fetch (`https://randomuser.me/api/?results=${users}`);
        const {results} = await response.json();
        let usersArray = [];


        for (const user of results) {
            const picture = user.picture.large;
            const { first, last } = user.name;
            const gender = user.gender;
            const {country} = user.location;
            const email = user.email;
            const {username} = user.login;

            usersArray.push(picture, first, last, gender, country, email, username); 
            
        }
        
        console.log(usersArray);

        
      



    } catch (error) {
        console.log(error);
    }
}


fetchUrl(6);
