'use strict';

// Mostrar la hora actualizada segundo a segundo por consola.
const interval = setInterval(() => {
    const currentDate = new Date();

    const day = currentDate.getDay();
    const hour = currentDate.getHours();
    const minute = currentDate.getMinutes();
    const second = currentDate.getSeconds();

    console.log(`${hour}:${minute}:${second} y llevamos ${day} día(s)`);
}, 5000);

